export default {
  namespace: 'dw',
  debug: Boolean(process.env.DEBUG_ECOM_NUXT)
}
