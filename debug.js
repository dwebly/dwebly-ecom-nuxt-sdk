// debug.js
const options = require('./config/var').default
const { debug, namespace } = options
if (debug) {
  console.log(`[${namespace}] options: `, options)
}
