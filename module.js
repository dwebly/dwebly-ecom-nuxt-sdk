/* eslint-disable node/no-path-concat */
const { readdirSync } = require('fs')
const { resolve, join, extname } = require('path')
const _options = require('./config/var').default

export default function (moduleOptions) {
  const options = {
    ...moduleOptions,
    ..._options
  }
  const { namespace } = options

  // add all of the initial plugins
  const pluginsToSync = [
    'debug.js',
    'plugins/index.js',
    'plugins/globalMixin.js',
    'plugins/lodash.js',
    'store/index.js'
  ]
  for (const pluginPath of pluginsToSync) {
    let pathString;
    let extra = {};
    if (typeof pluginPath === 'string') {
      pathString = pluginPath
    } else {
      const { src, ..._opts } = pluginPath
      pathString = src
      extra = _opts
    }

    this.addPlugin({
      src: resolve(__dirname, pathString),
      fileName: join(namespace, pathString),
      options,
      ...extra
    })
  }

  // sync all of the files and folders to revelant places in the nuxt build dir (.nuxt/)
  const foldersToSync = [
    'config',
    'plugins/lib',
    'store/modules/package'
  ]
  for (const pathString of foldersToSync) {
    const path = resolve(__dirname, pathString)

    const readdirAsync = (path, currPathString) => {
      readdirSync(path, { withFileTypes: true })
        .forEach((file) => {
          const { name } = file

          if (!file.isDirectory()) {
            this.addTemplate({
              src: resolve(__dirname, currPathString, name),
              fileName: join(namespace, currPathString, name),
              options
            })
          } else {
            // import from sub directory - recursively:
            readdirAsync(resolve(__dirname, currPathString, name), join(currPathString, name))
          }
        })
    }
    readdirAsync(path, pathString)
  }

  // generate page routes of pages files/directories from 'pages'
  const pagesRoutes = {}
  const readdirPagesAsync = (path, currPathString) => {
    readdirSync(path, { withFileTypes: true })
      .sort((a, b) => {
        const aName = !a.isDirectory() && a.name === 'index.vue'
          ? '_.vue' // ensure index route stay at top when registering
          : a.name

        const bName = !b.isDirectory() && b.name === 'index.vue'
          ? '_.vue' // ensure index route stay at top when registering
          : a.name

        if (!a.isDirectory() && !b.isDirectory()) {
          if (aName.startsWith('_') && aName !== '_.vue') {
            return -1 // ensure dynamic route stay at bottom when registering
          }
        }

        return (b.isDirectory() - a.isDirectory()) || (aName > bName ? 1 : -1)
      })
      .forEach((file) => {
        const { name } = file

        if (!file.isDirectory()) {
          if (extname(file.name).toLowerCase() === '.vue') {
            const dir = currPathString.split('/').slice(1)
            let fileName = name.startsWith('_') ? name.replace(name[0], ':') : name
            fileName = fileName.substring(fileName.lastIndexOf('/') + 1, fileName.lastIndexOf('.'))

            const path = `${dir.length ? '/' : ''}${dir.map(name => name.startsWith('_') ? name.replace(name[0], ':') : name).join('/')}${fileName !== 'index' ? `/${fileName}` : ''}`

            pagesRoutes[path] = {
              path,
              component: resolve(__dirname, currPathString, name)
            }
          }
        } else {
          // import from sub directory - recursively:
          readdirPagesAsync(resolve(__dirname, currPathString, name), join(currPathString, name))
        }
      })
  }
  readdirPagesAsync(resolve(__dirname, 'pages'), 'pages')

  // register page routes
  this.extendRoutes((routes) => {
    for (const name in pagesRoutes) {
      routes.push({
        path: pagesRoutes[name].path,
        component: pagesRoutes[name].component
      })
    }
  })
}

module.exports.meta = require('./package.json')
