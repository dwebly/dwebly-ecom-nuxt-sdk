const sleep = (milliseconds) => {
  const date = Date.now()
  let currentDate = null
  do {
    currentDate = Date.now()
  } while (currentDate - date < milliseconds)
}

// ~/plugin/api.js
const newToken = null
const auth = async (ctx) => {
  const { app, store, error, $cookies } = ctx
  return await store.dispatch('dw/authentication/token', {
    data: {
      grant_type: 'client_credentials',
      client_id: app.$config.EAPI_PASSPORT_CLIENT_ID,
      client_secret: app.$config.EAPI_PASSPORT_CLIENT_SECRET
    }
  })
    .then(async (response) => {
      const data = {
        guest: 1,
        domain: app.$config.STORE_DOMAIN_ID,
        subdomain: app.$config.STORE_SUBDOMAIN
      }
      return await store.dispatch('dw/authentication/customers/login', {
        data,
        headers: { Authorization: `Bearer ${response?.access_token}` }
      }).then( (user) => {
        if (typeof user.accessToken === 'string') {
          $cookies.set('auth._token', user.accessToken, {
            path: '/',
            maxAge: 60 * 60 * 24 * 3
          })
          return user.accessToken
        }
        return
      })

    })
    .catch((err) => {
      console.error(err)
      return error({ statusCode: 404, message: 'Page not found' })
    })
}

const repoWithAxios = (ctx, baseUrl) => {
  const { $axios, $cookies, store } = ctx;

  const api = $axios.create({
    headers: {
      common: {
        Accept: 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    }
  })

  api.interceptors.request.use(async (config) => {
    const url = config?.url
    if (((url || '').split('?'))[0] === '/oauth/token' || ((url || '').split('?'))[0] === '/auth/customer/login') { return config }
    while (process.server && store.state?.dw?.authentication?.customers?.fetching) {
      sleep(1000)
      console.log('waiting')
    }
    const cookie = $cookies.get('auth._token')
    if (!cookie) {
      store.dispatch('dw/authentication/customers/setAuthFetching', true)
      const token = await auth(ctx)
      config.headers.Authorization = `Bearer ${token}`
      store.dispatch('dw/authentication/customers/setAuthFetching', false)
    } else {
      const token = cookie
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  })

  api.interceptors.response.use( response => response,
    (error) => {
      const status = error.response ? error.response.status : null
      const errorUrl = error?.config?.url
      console.group('ERROR')
      console.log(`${error?.config?.method}: ${((errorUrl || '').split('?'))[0]}`)
      console.log('STATUS: ', error?.response?.status)
      console.log('HEADERS: ', error?.config?.headers)
      console.log('DATA: ', error?.response?.data)
      console.groupEnd('Error')

      if (status === 401 && ((errorUrl || '').split('?'))[0] !== '/auth/customer/login') {
        auth(ctx)
        .then( () => {
          if (process.client) { window.location.reload() }
          return $axios.request(error.config)
        })
      } else {
        return Promise.reject(error)
      }
    }
  )

  api.setBaseURL(baseUrl)

  return api
}

export default (ctx, inject) => {
  ctx.$axios.onRequest((config) => {
    const token = ctx.$cookies.get('auth._token')
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
  })

  // Create a custom axios instance
  inject('api', repoWithAxios(ctx, ctx.env.EAPI_URL))
  inject('eapi', repoWithAxios(ctx, ctx.env.EAPI_URL + '/api'))
  inject(
    'notificationApi',
    repoWithAxios(ctx, ctx.env.NOTIFICATION_API_URL + '/api')
  )
  inject('pluginApi', repoWithAxios(ctx, ctx.env.ECOM_PLUGIN_URL + '/api'))
}
