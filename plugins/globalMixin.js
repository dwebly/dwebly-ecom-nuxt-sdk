// plugins/auth.js
import Vue from 'vue'

Vue.mixin({
  mounted () {
    const token = this.$cookies?.get('auth._token')
    if (token && this.$auth.user === null && !this._.get(this.$store.state.dw, 'authentication.customers.fetchingUser')) {
      this.$store.dispatch('dw/authentication/customers/setUserFetching', true)
      this.$store.dispatch('dw/authentication/user').then( () => {
        this.$store.dispatch('dw/authentication/customers/setUserFetching', false)
      })
    }
  },
  computed: {
    $auth () {
      const logout = async () => {
        this.$cookies.remove('auth._token', {})
        await this.$store.dispatch('dw/authentication/logout')
        this.$router.push('/')
      }

      const strategy = {
        token: {
          get: () => {
            const token = this.$cookies.get('auth._token')
            return `Bearer $${token}`
          }
        }
      }

      const user = this.$store.state.dw?.authentication?.customers

      return { ...user, strategy, logout }
    }
  }
})
