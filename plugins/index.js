// plugins/index.js
import helpers from './lib/helpers'
import utilities from './lib/utilities'

const options = require('../config/var').default

// extract the namespace from the options
const { namespace } = options

// create the plugin
export default (ctx, inject) => {
  // inject helpers
  inject(namespace, { helpers, utilities })
}
