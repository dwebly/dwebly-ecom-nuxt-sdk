export default {
  getCategory (title) {
    // Check if data retrieve multiple category slugs
    if (Array.isArray(title)) {
      title = title.filter((x) => x !== null)
      title = title.join(', ')
    }
    if (title) {
      const words = title.split('-')
      for (let i = 0; i < words.length; i++) {
        words[i] = words[i][0].toUpperCase() + words[i].substr(1)
      }
      return words.join(' ')
    }
    return title
  }
}
