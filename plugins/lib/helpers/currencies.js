export default {
  display (price, currencies) {
    return currencies.code + ' ' + price.toFixed(currencies.decimal);
  },
  getSymbol (currencies) {
    return (currencies.symbol) ? currencies.symbol : currencies.code
  }
}
