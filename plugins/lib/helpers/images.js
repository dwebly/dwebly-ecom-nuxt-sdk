export default {
  getUserPropic (userImage, userAvatar = null, variation = 'md') {
    return (userImage == null) ? (userAvatar ?? '/img/placeholders/avatar.png') : this.getImageVariation(userImage, variation);
  },
  getProductImage (productImage, variation = 'md') {
    const response = (productImage == null) ? '/img/placeholders/product.png' : this.getImageVariation(productImage, variation);
    return response === null ? '/img/placeholders/product.png' : response;
  },
  getImageVariation (image, variation = 'md'){
    return ((image.variations == null) ? image.url : image.variations[variation]);
  }
}

/**
 * @todo improve this
 * ? this should be at swift instead
 */
