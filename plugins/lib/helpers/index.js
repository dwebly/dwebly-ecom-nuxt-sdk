import currencies from './currencies';
import images from './images';
import variants from './variants';
import products from './products';
import collections from './collections';
import locations from './locations';
import urls from './urls';

export default {
  currencies,
  images,
  variants,
  products,
  collections,
  locations,
  urls
}