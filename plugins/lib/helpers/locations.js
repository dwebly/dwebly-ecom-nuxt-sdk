export default {
  locationCode (location) {
    let locationCode = ''
    if (location.countryCode !== null) {
      locationCode = locationCode + location.countryCode
    }
    if (location.divisionCode !== null) {
      locationCode = locationCode + '-' + location.divisionCode
    }
    if (location.cityId !== null) {
      locationCode = locationCode + '-' + location.cityId
    }
    return locationCode
  },

  getCountryCode (countryName) {
      countryName = countryName.toLowerCase()
      switch (countryName) {
          case 'malaysia':
              return 'my'
          case 'thailand':
              return 'th'
          case 'international':
              return 'intl'
          default:
              return 'intl';
      }
  },

  getCountryName (countryCode) {
      countryCode = countryCode.toLowerCase()
      switch (countryCode) {
          case 'my':
              return 'malaysia'
          case 'th':
              return 'thailand'
          case 'intl':
              return 'international'
          default:
              return 'international';
      }
  }
}
