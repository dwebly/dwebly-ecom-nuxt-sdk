import { get } from 'lodash'
export default {

  /**
   * @todo convert to human readable function
   * @todo optimize some function
   */

  // PRICE WITHOUT CURRENCY IN DECIMAL
  getPriceFloat ( product, variantId = null ) {
    if (product.order_variant_id){
      return parseFloat(product.price.toFixed(product.currency.decimal))
    } else if (product.cart_item_id){
      return parseFloat(product.variant.price.toFixed(product.variant.currency.decimal))
    } else if( product.default ){
      return parseFloat(product.default.price.toFixed(product.default.currency.decimal))
    } else if( product?.variants?.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null : parseFloat(variant.price.toFixed( variant.currency.decimal))
      } else {
        return parseFloat(product.variants[0].price.toFixed(product.variants[0].currency.decimal))
      }
    }
    return null
  },
  
  getSalePriceFloat ( product, variantId = null ) {
    if (product.order_variant_id){
      return parseFloat(product.sale_price.toFixed(product.currency.decimal))
    } else if (product.cart_item_id){
      return parseFloat(product.variant.sale_price.toFixed(product.variant.currency.decimal))
    } else if( product.default ){
      return parseFloat(product.default.sale_price.toFixed(product.default.currency.decimal))
    } else if( product.variants.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null :  parseFloat(variant.sale_price.toFixed( variant.currency.decimal))
      } else {
        return parseFloat(product.variants[0].sale_price.toFixed(product.variants[0].currency.decimal))
      }
    }
    return null
  },

  // PRICE WITH CURRENCY
  getPrice ( product, variantId = null ) {
    if (product.order_variant_id){
      return product.currency.code + ' ' + product.price.toFixed(product.currency.decimal)
    } else if (product.cart_item_id){
      return product.variant.currency.code + ' ' + product.variant.price.toFixed(product.variant.currency.decimal)
    } else if( product.default ){
      return product.default.currency.code + ' ' + product.default.price.toFixed(product.default.currency.decimal)
    } else if( product?.variants?.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null :  variant.currency.code + ' ' +  variant.price.toFixed( variant.currency.decimal)
      } else {
        return product.variants[0].currency.code + ' ' + product.variants[0].price.toFixed(product.variants[0].currency.decimal)
      }
    }
    return null
  },

  // Check if price not 0 or float
  priceCheck (product) {
    const minCheck = parseFloat(product.price_range.min)
    const maxCheck = parseFloat(product.price_range.max)
    if (isNaN(minCheck) || isNaN(maxCheck)) { return false }
    return (minCheck > 0 && maxCheck > 0)
  },

  // @TODO Pending refactor getPriceInstallment to take sale price
  getPriceInstallment ( product, installment = 2 ) {
    if (product.order_variant_id){
      const price = product.price / installment;
      return product.currency.code + ' ' + price.toFixed(product.currency.decimal)
    } else if (product.cart_item_id){
      const price = product.variant.price / installment;
      return product.variant.currency.code + ' ' + price.toFixed(product.variant.currency.decimal)
    } else if( product.default ){
      const price = product.default.price / installment;
      return product.default.currency.code + ' ' + price.toFixed(product.default.currency.decimal)
    } else if( product?.variants?.length > 0 ){
      const price = product.variants[0].price / installment;
      return product.variants[0].currency.code + ' ' + price.toFixed(product.variants[0].currency.decimal)
    }
    return null
  },

  getSalePriceInstallment ( product, installment = 2 ) {
    if (product.order_variant_id){
      const price = product.sale_price / installment;
      return product.currency.code + ' ' + price.toFixed(product.currency.decimal)
    } else if (product.cart_item_id){
      const price = product.variant.sale_price / installment;
      return product.variant.currency.code + ' ' + price.toFixed(product.variant.currency.decimal)
    } else if( product.default ){
      const price = product.default.sale_price / installment;
      return product.default.currency.code + ' ' + price.toFixed(product.default.currency.decimal)
    } else if( product?.variants?.length > 0 ){
      const price = product.variants[0].sale_price / installment;
      return product.variants[0].currency.code + ' ' + price.toFixed(product.variants[0].currency.decimal)
    }
    return null
  },

  getPriceRange ( product ) {
    const currency = get(product, 'price_range.currency', '') || ''
    const min = parseFloat(get(product, 'price_range.min', 0)).toFixed(currency.decimal) || ''
    const max = parseFloat(get(product, 'price_range.max', 0)).toFixed(currency.decimal) || ''

    return min !== max
      ? currency.code + ' ' + min + ' - ' + currency.code + ' ' + max
      : currency.code + ' ' + min
  },

  onSale ( product, variantId = null ) {
    if (product.order_variant_id){
      return product.on_sale
    } else if (product.cart_item_id){
      return product.variant.on_sale
    } else if( product.default ){
      return product.default.on_sale
    } else if( product?.variants?.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null :  variant.on_sale
      } else {
        return product.variants[0].on_sale
      }
    }
    return null
  },

  getSalePrice ( product, variantId = null ) {
    if (product.order_variant_id){
      return product.currency.code + ' ' + product.sale_price.toFixed(product.currency.decimal)
    } else if (product.cart_item_id){
      return product.variant.currency.code + ' ' + product.variant.sale_price.toFixed(product.variant.currency.decimal)
    } else if( product.default ){
      return product.default.currency.code + ' ' + product.default.sale_price.toFixed(product.default.currency.decimal)
    } else if( product.variants.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null :  variant.currency.code + ' ' +  variant.sale_price.toFixed( variant.currency.decimal)
      } else {
        return product.variants[0].currency.code + ' ' + product.variants[0].sale_price.toFixed(product.variants[0].currency.decimal)
      }
    }
    return null
  },


  // PRODUCT SUMMARY
  getSummary ( product, variantId = null ) {
    if( product.default ){
      return product.default.summary
    } else if( product.variants.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null : variant.summary
      } else {
        return product.variants[0].summary
      }
    }
    return null
  },

  // PRODUCT DESCRIPTIONS
  getDescription ( product, variantId = null ) {
    if( product.default ){
      return product.default.description
    } else if( product.variants.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null : variant.description
      } else {
        return product.variants[0].description
      }
    }
    return null
  },

  // PRODUCT ADDITIONAL INFO
  getAddInfo ( product, variantId = null ) {
    if( product.default ){
      return product.default.add_info
    } else if( product.variants.length > 0 ){
      if ( variantId ) {
        const variant = product.variants.find(x => x.variant_id === variantId.toString())
        return (typeof variant === 'undefined') ? null : variant.add_info
      } else {
        return product.variants[0].add_info
      }
    }
    return null
  }

}
