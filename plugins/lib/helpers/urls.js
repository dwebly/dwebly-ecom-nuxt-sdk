export default {
  getQueryStringParams (query) {
    return query
      ? (/^[?#]/.test(query) ? query.slice(1) : query)
          .split('&')
          .reduce((params, param) => {
            const [key, value] = param.split('=')
            params[key] = value
              ? decodeURIComponent(value.replace(/\+/g, ' '))
              : ''
            return params
          }, {})
      : {}
  },

  convertParamsToPath (params) {
    const parameters = []
    for (const property in params) {
      if (Object.prototype.hasOwnProperty.call(params, property)) {
        parameters.push(encodeURI(property + '=' + params[property]))
      }
    }
    return parameters.join('&')
  },

  embeddingPathStorePro (data) {
    let response = data
    if (data === null) {
      response = '#'
    } else if (!['/', '#'].includes(data.charAt(0))) {
      response = '/' + response
    }
    return response
  }
}
