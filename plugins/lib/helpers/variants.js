export default {
  getVariantPrice (variant, withCurrency = true) {
    const price = (variant.on_sale ? variant.sale_price : variant.price).toFixed(variant.currency.decimal)
    const prefix = withCurrency ? variant.currency.code : null
    return prefix ? prefix + ' ' + price : price
  },
  getVariantSubtotal (variant, withCurrency = true) {
    const price = variant.on_sale ? variant.sale_price : variant.price
    const subtotal = (price * variant.quantity).toFixed(variant.currency.decimal)
    const prefix = withCurrency ? variant.currency.code : null
    return prefix ? prefix + ' ' + subtotal : subtotal
  },
  getCssColorValue (value){
    const css = 'background:';
    if(Object.keys(value.meta).length !== 0){

      if(value.meta.custom_colorValue2){
        return css + 'linear-gradient(90deg, ' + value.value + ' 50%, ' + value.meta.custom_colorValue2 +' 50%)'
      }else{
        return css + value.value
      }

    } else if( /^#([0-9A-F]{3}){1,2}$/i.test(value.value) ){
      return css + value.value
    } else {
      return css + ' linear-gradient(90deg, #FF0000 0%, #FFFF00 20%, #00FF00 40%, #00FFFF 60%, #0000FF 80%, #000000 100%);'
    }
  }
}
