// store/index.js
const { get } = require('lodash')
const options = require('../config/var').default
// extract the namespace var
const { namespace } = options

function requireAll (requireContext) {
  return requireContext.keys().reduce((previous, current) => {
    let component = requireContext(current)
    component = component.default || component
    const storeName = component(options).storeName

    if (storeName) {
      if (!previous[storeName]) {
        previous[storeName] = component
      } else {
        // Duplication 'storeName':
        throw new Error(`[${namespace} vuex] 'storeName' of "${storeName}" already exist! './store/modules/${current.replace(/^\.\//, '')}'`)
      }
    } else {
      // Missing 'storeName':
      throw new Error(`[${namespace} vuex] Missing 'storeName'! './store/modules/${current.replace(/^\.\//, '')}'`)
    }
    return previous
  }, {})
}

let storeModules = requireAll(require.context('./modules', true, /\.js$/))

// sort object by key name ascending
storeModules = Object.keys(storeModules).sort().reduce(
  (obj, key) => {
    obj[key] = storeModules[key]
    return obj
  },
  {}
)

// create the plugin
export default ({ store }, inject) => {
  // loop through components and register them
  for (const name in storeModules) {
    if (name && storeModules[name]) {
      // this will be overwritten by root 'name' store module (if any)
      const storeName = name.split('.')
      store.registerModule(storeName, storeModules[name](options), {
        preserveState: Boolean(get(store.state, storeName.join('.'))) // if the store module already exists, preserve it
      })
    }
  }
}
