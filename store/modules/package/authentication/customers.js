// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/auth/customer'

export default (options) => ({
  storeName: options.namespace + '.authentication.customers', // REQUIRED
  namespaced: true,
  state: () => ({
    user: null,
    loggedIn: false,
    fetching: false,
    fetchingUser: null
  }),

  mutations: {
    // mutations:
    SET_AUTH_FETCHING (state, data) {
      state.fetching = data
    },
    SET_USER_FETCHING (state, data) {
      state.fetchingUser = data
    },
    SET_LOGGED_IN_USER (state, user) {
      state.loggedIn = true
      state.user = user
    },
    LOGGED_OUT_USER (state) {
      state.loggedIn = false
      state.user = null
    }
  },

  actions: {
    login ({ dispatch }, config) {
      const { data, headers, slug = null } = config
      console.log('...[Dw] Login user from api. Guest:', !!data?.guest)
      return this.$eapi
        .$post(
          resourceURL + '/login' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json', ...headers } }
        )
        .then((response) => {
          dispatch('updateLoggedInUser', response.data)
          return response.data
        })
        .catch((error) => { throw error })
    },

    updateLoggedInUser ({ commit }, user) {
      if (!user) {
        this.$cookies.removeAll()
        commit('LOGGED_OUT_USER')
        return
      }

      const { accessToken, expires_in: expiresIn, ..._user } = user
      if (accessToken) {
        this.$cookies.set('auth._token', accessToken, { path: "/", maxAge: expiresIn })
      }
      if (expiresIn) {
        this.$cookies.set('auth._token_expiration', expiresIn, { path: "/", maxAge: expiresIn })
      }
      this.$cookies.set('user.guest', !!_user.guest, { path: "/", maxAge: expiresIn })
      commit('SET_LOGGED_IN_USER', _user)
    },

    // POST api/auth/customer/change-password
    changePassword ({}, config) {
      const { data, slug = null } = config
      console.log('...[Dw] Change password from api')

      return this.$eapi
        .$post(
          resourceURL + '/change-password' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    },

    // POST api/auth/customer/forgot-password
    forgotPassword ({}, config) {
      const { data, slug = null } = config
      return this.$eapi
        .$post(
          resourceURL + '/forgot-password' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    },

    // POST api/auth/customer/reset-password
    resetPassword ({}, config) {
      const { data, slug = null } = config
      return this.$eapi
        .$post(
          resourceURL + '/reset-password' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          return response
        })
        .catch((error) => {
          throw error
        })
    },

    setAuthFetching ({ commit }, data) {
      commit('SET_AUTH_FETCHING', data)
    },

    setUserFetching ({ commit }, data) {
      commit('SET_USER_FETCHING', data)
    },
  }
})
