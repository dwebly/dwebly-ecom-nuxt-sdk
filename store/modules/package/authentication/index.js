// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/oauth'

export default options => ({
  storeName: options.namespace + '.authentication', // REQUIRED
  namespaced: true,
  state: () => ({
    processing: false
  }),
  mutations: {
    // mutations:
    SET_PROCESSING (state, status) {
      state.processing = status
    }
  },
  actions: {
    // GET api/oauth/user
    user ({ dispatch }) {
      console.log('...[Dw] Fetch user from api')
      return this.$eapi
        .$get(resourceURL + '/user')
        .then((response) => {
          dispatch('dw/authentication/customers/updateLoggedInUser', response, { root: true })
          return response
        })
        .catch((error) => { throw error })
    },

    // GET api/oauth/logout
    logout ({ dispatch }) {
      console.log('...[Dw] Logout user from api')
      return this.$eapi
        .$get(resourceURL + '/logout')
        .then((response) => {
          dispatch('dw/authentication/customers/updateLoggedInUser', null, { root: true })
          return response
        })
        .catch((error) => { throw error })
    },

    // GET api/oauth/token
    token ({}, config) {
      const { data, slug = null } = config
      // console.log('...[Dw] Get oauth token')
      return this.$api
        .$post(
          '/oauth/token' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    },

    // Set auth processing status
    setProcessing ({ commit }, status) {
      commit('SET_PROCESSING', status)
    }
  }
})
