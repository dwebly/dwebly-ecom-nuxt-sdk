// store/modules/counter.js
/* eslint-disable no-empty-pattern */
const resourceURL = '/banks'
export default options => ({
  storeName: options.namespace + '.banks', // REQUIRED
  namespaced: true,
  state: () => ({
    banks: null
  }),
  mutations: {
    SET_BANKS (state, banks) { state.banks = banks }
  },
  actions: {
    // api/banks
    fetchAll ({ state, commit }, config) {
      const { slug = null } = config
      const banks = state.banks
      if (banks) {
        console.log('...Fetch banks from state')
        return banks
      } else {
        console.log('...Fetch banks from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_BANKS', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    }
  },
  getters: {
    getBanks: (state) => state.banks
  }
})
