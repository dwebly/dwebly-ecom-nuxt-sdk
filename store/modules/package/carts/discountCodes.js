// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/cart/discountCodes'

export default options => ({
  storeName: options.namespace + '.carts.discountCodes', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // POST api/carts/discountCodes/{id}
    add ({ commit }, config) {
      const { data, slug = null } = config
      console.log('... Applying discount codes')
      return this.$eapi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          commit('dw/carts/SET_CART', response.data, { root: true })
          return response.data
        })
        .catch((error) => { throw error })
    },

    // DELETE api/carts/discountCodes/{id}
    remove ({ commit }, config) {
      const { id, data, slug = null } = config
      console.log('...Removing discount codes')
      return this.$eapi
        .$delete(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          commit('dw/carts/SET_CART', response.data, { root: true })
          return response.data
        })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
