// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/carts'

export default options => ({
  storeName: options.namespace + '.carts', // REQUIRED
  namespaced: true,
  state: () => ({
    cart: null,
    cartInitiate: false
  }),
  mutations: {
    SET_CART (state, cart) {
      state.cart = cart
    },
    UPDATE_CART_INITIATE (state, status) {
      state.cartInitiate = status
    }
  },
  actions: {
    // GET api/carts/{id}
    fetch ({ state, commit }, config) {
      const { id, slug = null } = config
      const cart = state.cart;
      if (cart) {
        console.log('...Fetch cart from state')
        return cart
      } else {
        console.log('...Fetch cart from api')
        return this.$eapi
          .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_CART', response.data)
            commit('UPDATE_CART_INITIATE', true)

            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // PUT api/carts/{id}
    update ({ commit }, config) {
      const { id, data, slug = null } = config
      console.log('...Update cart from api')
      return this.$eapi
        .$put(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          commit('SET_CART', response.data)

          return response.data
        })
        .catch((error) => { throw error })
    },

    // DELETE api/carts/{id}
    remove ({ commit }, config) {
      const { id, slug = null } = config
      console.log('...delete cart from api')
      return this.$eapi({
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        url: resourceURL + '/' + id + (slug !== null ? '?' + slug : '')
      })
        .then((response) => {
          console.log('...remove cart state')
          commit('SET_CART', null)
          return response.data
        })
        .catch((error) => { throw error })
    },

    store ({ commit }, config) {
      const { data, slug = null } = config
      // create new cart
      console.log('...Create carts from api')
      return this.$eapi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          console.log('...save cart state')
          commit('SET_CART', response.data)
          commit('UPDATE_CART_INITIATE', true)

          return response.data
        })
        .catch((error) => { throw error })
    },

    /**
     * Function side
     */
    setCartInitiate ({ commit }, status) {
      console.log('...save cart initiate')
      commit('UPDATE_CART_INITIATE', status)
    }
  },
  getters: {}
})
