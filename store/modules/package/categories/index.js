// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/categories'

export default options => ({
  storeName: options.namespace + '.categories', // REQUIRED
  namespaced: true,
  state: () => ({
    categories: []
  }),
  mutations: {
    SET_CATEGORIES (state, categories) {
      state.categories = categories
    }
  },
  actions: {
    // api/categories
    fetchAll ({ state, commit }, config) {
      const { slug = null } = config
      const categories = state.categories
      if (categories.length && slug === null) {
        console.log('...Fetch categories from state')
        return categories
      } else {
        console.log('...Fetch categories from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_CATEGORIES', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // GET api/categories/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      const category = this.getters['dw/categories/getCategoryById'](id)
      if (category) {
        console.log('...Fetch category from state')
        return category
      } else {
        console.log('...Fetch category from api')
        return this.$eapi
          .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
          .then((response) => { return response.data })
          .catch((error) => { throw error })
      }
    }
  },
  getters: {
    getCategories: (state) => state.categories,
    getCategoryById: (state) => (id) => {
      const categories = JSON.parse(JSON.stringify(state.categories))
      const findCategory = categories.find(x => x.category_id === id.toString())
      return (typeof findCategory === 'undefined') ? false : findCategory
    }
  }
})
