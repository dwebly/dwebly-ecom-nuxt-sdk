// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/channel/storepros'

export default options => ({
  storeName: options.namespace + '.channels.storepros', // REQUIRED
  namespaced: true,
  state: () => ({
    storepro: null,
    pending: false
  }),
  mutations: {
    SET_STOREPRO (state, storepro) { state.storepro = storepro },
    SET_PENDING (state, pending) { state.pending = pending }
  },
  actions: {
    // api/channels/storepros
    fetch ({ state, commit }, config) {
      const { id, slug = null } = config
      const storepro = state.storepro
      if (storepro) {
        console.log('...Fetch channel storepro from state')
        return storepro
      } else {
        console.log('...Fetch channel storepro from api')
        commit('SET_PENDING', true)
        return this.$pluginApi
          .$get(resourceURL + '/' + id  + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_STOREPRO', response.data)
            commit('SET_PENDING', false)
            return response.data
          })
          .catch((error) => {
            commit('SET_PENDING', false)
            throw error
          })
      }
    }
  },
  getters: {
    getStorepro: (state) => state.storepro
  }
})
