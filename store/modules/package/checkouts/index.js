// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/checkouts'

export default options => ({
  storeName: options.namespace + '.checkouts', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // POST api/checkouts
    store ({}, config) {
      const { data, slug = null } = config
      return this.$eapi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    fetch ({},  config) {
      const { id, slug = null } = config
      console.log('...Fetch checkout cart from api')
      return this.$eapi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
