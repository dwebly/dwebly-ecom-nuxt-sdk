// store/modules/counter.js
/* eslint-disable no-empty-pattern */

export default options => ({
  storeName: options.namespace + '.deferred', // REQUIRED
  namespaced: true,
  state: () => ({
    jobs: [],
    running: false
  }),
  mutations: {
    start (state) {
      state.running = true;
    },
    stop (state) {
      state.running = false;
      state.jobs = [];
    },
    defer (state, { job, payload }) {
      state.jobs.push({ job, payload });
    }
  },
  actions: {
    async run ({ state, dispatch, commit }) {
      if (!process.client) {
        throw new Error('deferred queue should never be triggered from a SSR');
      }
      if (!state.running && state.jobs.length) {
        commit('start');
        const runJob = ({ job, payload }) => dispatch(job, payload, { root: true });
        try {
          await Promise.allSettled(state.jobs.map(runJob));
        } finally {
          commit('stop');
        }
      }
    },
    // Receive a job to execute client side only, it will execute immediately if
    // running client-side, otherwise it is queued
    async clientOnly ({ commit, dispatch }, { job, payload }) {
      if (process.server) {
        commit('defer', { job, payload });
      } else {
        return await dispatch(job, payload, { root: true });
      }
    }
  },
  getters: {
    isDeferred: ({ jobs, running }) => running || !!jobs.length
  }
})
