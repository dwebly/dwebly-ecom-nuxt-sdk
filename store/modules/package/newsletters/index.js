// store/modules/counter.js
/* eslint-disable no-empty-pattern */

export default options => ({
  storeName: options.namespace + '.newsletters', // REQUIRED
  namespaced: true,
  state: () => ({
    remain_close: false
  }),
  mutations: {
    SET_REMAIN_CLOSE (state, close) {
      state.remain_close = close
    }
  },
  actions: {
    /**
     * Function side
     */
    setRemainClose ({ commit }, close) {
      commit('SET_REMAIN_CLOSE', close)
    }
  },
  getters: {}
})
