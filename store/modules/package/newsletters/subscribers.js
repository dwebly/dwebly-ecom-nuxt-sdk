// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/newsletter/subscribers'

export default options => ({
  storeName: options.namespace + '.newsletters.subscribers', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // POST api/newsletter/subscribers
    store ({}, config) {
      const { data, slug = null } = config
      return this.$eapi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
