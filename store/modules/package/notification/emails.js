// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/emails'

export default options => ({
  storeName: options.namespace + '.notification.emails', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // POST api/emails
    store ({}, config) {
      const { data, slug = null } = config
      return this.$notificationApi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
