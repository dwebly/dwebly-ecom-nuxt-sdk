// store/modules/counter.js
/* eslint-disable no-empty-pattern */

export default options => ({
  storeName: options.namespace + '.notification', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {},
  getters: {}
})
