// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/orders'

export default options => ({
  storeName: options.namespace + '.orders', // REQUIRED
  namespaced: true,
  state: () => ({
    orders: []
  }),
  mutations: {
    SET_ORDERS (state, orders) {
      state.orders = orders
    }
  },
  actions: {
    // api/orders
    fetchAll ({ state, commit }, config) {
      const { slug = null } = config
      const orders = state.orders
      if (orders.length) {
        console.log('...Fetch orders from state')
        return orders
      } else {
        console.log('...Fetch orders from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_ORDERS', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // GET api/orders/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      const order = this.getters['dw/orders/getOrderById'](id)
      if (order) {
        console.log('...Fetch order from state')
        return order
      } else {
        console.log('...Fetch order from api')
        return this.$eapi
          .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
          .then((response) => { return response.data })
          .catch((error) => { throw error })
      }

    }
  },
  getters: {
    getOrders: (state) => state.orders,
    getOrderById: (state) => (id) => {
      const order = JSON.parse(JSON.stringify(state.orders))
      const findOrder = order.find(x => x.order_id === id.toString())
      return (typeof findOrder === 'undefined') ? false : findOrder
    }
  }
})
