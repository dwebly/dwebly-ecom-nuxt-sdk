// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/order/receipts'

export default options => ({
  storeName: options.namespace + '.orders.receipts', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: { },
  actions: {
    // GET api/orders/{id}
    fetch ({}, config) {
      const { id, slug = null } = config

      console.log('...Fetch receipt from api')
      return this.$eapi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
