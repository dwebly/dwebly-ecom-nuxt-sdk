// store/modules/counter.js
/* eslint-disable no-empty-pattern */

export default options => ({
  storeName: options.namespace + '.popup', // REQUIRED
  namespaced: true,
  state: () => ({
    share: false,
    shareLink: null,
  
    sideMenu: false,
  
    aboutStore: false,
    aboutStoreContent: false
  }),
  mutations: {
    TOGGLE_POPUP (state, target) {
      state[target] = !state[target]
    },
    SET_SHARE_LINK (state, link) {
      state.shareLink = link
    },
    SET_ABOUT_STORE_CONTENT (state, content) {
      state.aboutStoreContent = content
    }
  },
  actions: {

    // FUNCTION SIDE
    toggleShare (context, link = null) {
      this.commit('popup/TOGGLE_POPUP', 'share')
      this.commit('popup/SET_SHARE_LINK', link)
    },

    toggleSideMenu (context) {
      this.commit('popup/TOGGLE_POPUP', 'sideMenu')
    },

    toggleAboutStore (context, content = null) {
      this.commit('popup/TOGGLE_POPUP', 'aboutStore')
      this.commit('popup/SET_ABOUT_STORE_CONTENT', content)
    }

  },
  getters: {}
})
