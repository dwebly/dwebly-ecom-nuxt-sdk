// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/posts'

export default options => ({
  storeName: options.namespace + '.posts', // REQUIRED
  namespaced: true,
  state: () => ({
    posts: null
  }),
  mutations: {
    SET_POSTS (state, posts) {
      state.posts = posts
    }
  },
  actions: {

    // api/posts
    fetchAll ({ commit }, config) {
      const { slug = null } = config
      const posts = this.getters['dw/posts/getposts']
      if(posts){
        console.log('...Fetch posts from state')
        return posts
      }else{
        console.log('...Fetch posts from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_POSTS', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // POST api/posts
    store ({}, config) {
      const { data, slug = null } = config
      return this.$eapi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    }

  },
  getters: {
    getPosts: (state) => state.posts
  }
})
