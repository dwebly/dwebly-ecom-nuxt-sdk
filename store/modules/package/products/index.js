// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/product'

export default options => ({
  storeName: options.namespace + '.products', // REQUIRED
  namespaced: true,
  state: () => ({
    products: [],
    selectedProductQuantity: null,
    selectedProductVariant: null,
    selectedProductVariantMetadata: null
  }),
  mutations: {
    SET_PRODUCTS (state, products) {
      state.products = products
    },
    SET_SELECTED_PRODUCT_QUANTITY (state, quantity) {
      state.selectedProductQuantity = quantity
    },
    SET_SELECTED_PRODUCT_VARIANT (state, variant) {
      state.selectedProductVariant = variant
    },
    SET_SELECTED_PRODUCT_VARIANT_METADATA (state, metadata) {
      state.selectedProductVariantMetadata = metadata
    }
  },
  actions: {
    // api/products
    fetchAll ({}, config) {
      const { slug = null, full = null } = config
      console.log('...Fetch products from api')
      return this.$eapi
        .$get(resourceURL + 's' + (slug !== null ? '?' + slug : ''))
        .then((response) => { return (full) ? response : response.data })
        .catch((error) => { throw error })
    },

    // GET api/products/{id}
    fetchById ({}, config) {
      const { id, slug = null } = config
      console.log('...Fetch product from api')
      return this.$eapi
        .$get(resourceURL + 's/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    // GET api/product/slug/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      console.log('...Fetch product from api')
      return this.$eapi
        .$get(resourceURL + '/slugs/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    // FUNCTION SIDE
    updateSelectedProductQuantity ({ commit }, quantity) {
      commit('SET_SELECTED_PRODUCT_QUANTITY', quantity)
      return quantity
    },

    updateSelectedProductVariant ({ commit }, variant) {
      commit('SET_SELECTED_PRODUCT_VARIANT', variant)
      return variant
    },

    updateSelectedProductVariantMetadata ({ commit }, metadata) {
      commit('SET_SELECTED_PRODUCT_VARIANT_METADATA', metadata)
      return metadata
    }
  },
  getters: {
    products: (state) => state.products,
    getProductBySlug: (state) => (slug) => {
      const product = JSON.parse(JSON.stringify(state.products))
      const findProduct = product.find(x => x.slug === slug.toString())
      return (typeof findProduct === 'undefined') ? false : findProduct
    }
  }
})
