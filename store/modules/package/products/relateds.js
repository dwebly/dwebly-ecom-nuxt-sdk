// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/product/relateds'

export default options => ({
  storeName: options.namespace + '.products.relateds', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // GET api/product/slug/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      console.log('...Fetch product related from api')
      return this.$eapi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
