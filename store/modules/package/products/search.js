// store/modules/counter.js
/* eslint-disable no-empty-pattern */

export default options => ({
  storeName: options.namespace + '.products.search', // REQUIRED
  namespaced: true,
  state: () => ({
    searchQuery: null
  }),
  mutations: {
    SET_SEARCH_QUERY (state, query) {
      state.searchQuery = query
    }
  },
  actions: {
    // FUNCTION SIDE
    updateQuery ({ commit }, query) {
      commit('SET_SEARCH_QUERY', query)
      return query
    }
  },
  getters: {}
})
