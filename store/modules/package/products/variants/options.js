// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/product/variant/options'

export default options => ({
  storeName: options.namespace + '.products.variants.options', // REQUIRED
  namespaced: true,
  state: () => ({
    options: []
  }),
  mutations: {
    SET_OPTIONS (state, options) { state.options = options }
  },
  actions: {
    // api/theme/storelite
    fetchALL ({ state, commit }, config) {
      const { slug = null } = config
      const options = state.options
      if (options) {
        console.log('...Fetch product variant options from state')
        return options
      } else {
        console.log('...Fetch product variant options from api')

        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_OPTIONS', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    }
  },
  getters: {
    getOptions: (state) => state.options
  }
})
