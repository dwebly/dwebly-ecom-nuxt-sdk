// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/receipt/orders'

export default options => ({
  storeName: options.namespace + '.receipts.orders', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // api/receipt/orders
    fetch ({}, config) {
      const { id, slug = null } = config
      console.log('...Fetch store setting from api')
      return this.$eapi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
