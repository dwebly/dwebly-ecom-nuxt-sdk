// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/analytics'

export default options => ({
  storeName: options.namespace + '.resellers.analytics', // REQUIRED
  namespaced: true,
  state: () => ({}),

  mutations: {},

  actions: {

    // api/posts
    fetchAll ({}, config) {
      const { slug = null } = config
      console.log('...Fetch posts from api')
      return this.$pluginApi
        .$get(resourceURL + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }

  },

  getters: {}
})

// @todo Add state for analytics