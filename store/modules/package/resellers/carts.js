// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/carts'

export default options => ({
  storeName: options.namespace + '.resellers.carts', // REQUIRED
  namespaced: true,
  state: () => ({
    carts: null,
    cart: null,
    cartInitiate: false,

    // Query
    request: null,
    query: null,
    query_loading: false
  }),

  mutations: {
    SET_CARTS (state, carts) { state.carts = carts },
    SET_CART (state, cart) { state.cart = cart },
    UPDATE_CART_INITIATE (state, status) { state.cartInitiate = status },
    REMOVE_CART_BY_ID (state, id) {
      if (state.carts === null ) { return }
      const index = state.carts.findIndex(x => x.id === id)
      if (index) {
        const carts = state.carts.splice(index, 1)
        state.carts = { ...carts }
      }
    },
    RESET_CART_STATE (state) { state.cart = null },

    // Query
    SET_QUERY_REQUEST (state, request) { state.request = request },
    SET_QUERY_DATA (state, query) { state.query = query },
    SET_QUERY_LOADING (state, loading) { state.query_loading = loading }
  },
  actions: {
    // api/reseller/carts
    fetchAll ({ commit }, config) {
      const { slug = null, full = false } = config
      console.log('...Fetch reseller carts from api')
      return this.$pluginApi
        .$get(resourceURL + (slug !== null ? '?' + slug : ''))
        .then((response) => {
          commit('SET_CARTS', response.data)
          return (full)? response : response.data
        })
        .catch((error) => { throw error })
    },

    // GET api/reseller/carts/{id}
    fetch ({ state, commit }, config) {
      const { id, slug = null } = config
      const cart = state.cart
      if (cart) {
        console.log('...Fetch cart from state')
        return cart
      } else {
        console.log('...Fetch cart from api')
        return this.$pluginApi
          .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_CART', response.data)
            commit('UPDATE_CART_INITIATE', true)
            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // PUT api/reseller/carts/{id}
    update ({ commit }, config) {
      const { id, data, slug = null } = config
      console.log('...Update cart from api')
      return this.$pluginApi
        .$put(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          commit('SET_CART', response.data)
          return response.data
        })
        .catch((error) => { throw error })
    },

    // DELETE api/reseller/carts/{id}
    remove ({ commit }, config) {
      const { id, slug = null } = config
      return this.$pluginApi({
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        url: resourceURL + '/' + id + (slug !== null ? '?' + slug : '')
      })
        .then( (response) => {
          console.log('...remove cart state')
          commit('SET_CART', null)
          commit('REMOVE_CART_BY_ID', id)
          return response
        })
        .catch(error => { throw error })
    },

    // POST api/reseller/carts/{id}
    store ({ commit }, config) {
      const { data, slug = null } = config
      console.log('...Create carts from api')
      return this.$pluginApi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          console.log('...save cart state')
          commit('SET_CART', response.data)
          commit('UPDATE_CART_INITIATE', true)

          return response.data
        })
        .catch((error) => { throw error })
    },

      /**
       * Function side
       */
      setCartInitiate ({ commit }, status) {
      console.log('...save cart initiate')
      commit('UPDATE_CART_INITIATE', status)
    },
    resetCartState ({ commit }) {
      console.log('...reset cart state')
      commit('RESET_CART_STATE')
    },

    // Query
    query ({ state, commit }, config) {
      const {  slug = null } = config
      const request = state.request
      if (request) {
        request.cancel('fetch cancelled')
        commit('SET_QUERY_REQUEST', null)
      }
      commit('SET_QUERY_LOADING', true)
      console.log('...Fetch reseller roles from api')
      const axiosSource = this.$pluginApi.CancelToken.source()
      commit('SET_QUERY_REQUEST', axiosSource)
      return this.$pluginApi
        .$get(
          resourceURL + (slug !== null ? '?' + slug : ''),
          { cancelToken: axiosSource.token }
        )
        .then((response) => {
          commit('SET_QUERY_LOADING', false)
          commit('SET_QUERY_DATA', response)
          commit('SET_QUERY_REQUEST', null)
          return response
        })
        .catch((error) => {
          commit('SET_QUERY_LOADING', false)
          throw error
        })
    }

  },

  getters: {
    getCart: (state) => (id) => {
      // check current handled cart
      if (state?.cart?.id === id) { return state.cart }
      // check all cart lists
      if ( state.carts ) { return state.carts.find(x => x.id === id) }
    }
  }
})
