// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/checkouts'

export default options => ({
  storeName: options.namespace + '.resellers.checkouts', // REQUIRED
  namespaced: true,
  state: () => ({}),

  mutations: {},

  actions: {
    // api/reseller/checkouts
    fetchAll ({}, config) {
      const { slug = null, full = null } = config
      return this.$pluginApi
        .$get(resourceURL + (slug !== null ? '?' + slug : ''))
        .then((response) => { return (full)? response : response.data })
        .catch((error) => { throw error })
    },

    // GET api/reseller/checkouts/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      return this.$pluginApi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })

    },

    // PUT api/reseller/checkouts/{id}
    store ({}, config) {
      const { data, slug = null } = config
      return this.$pluginApi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }

  },

  getters: {}
})
