// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/customers'

export default options => ({
  storeName: options.namespace + '.resellers.customers', // REQUIRED
  namespaced: true,
  state: () => ({
    customerTemplate: {
      email: null,
      password: null,
      password_confirmation: null,
      fname: null,
      lname: null,
      phone: null,
      countryCode: null,
      divisionCode: null,
      cityId: null,
      zipcode: null,
      address: null,
      location_code: null
    },

    // Query
    request: null,
    query: null,
    query_loading: false
  }),

  mutations: {
    // Query
    SET_QUERY_REQUEST (state, request) { state.request = request },
    SET_QUERY_DATA (state, query) { state.query = query },
    SET_QUERY_LOADING (state, loading) { state.query_loading = loading }
  },

  actions: {

    // GET api/reseller/checkouts/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      return this.$pluginApi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })

    },

    // PUT api/reseller/checkouts/{id}
    store ({}, config) {
      const { data, slug = null } = config
      return this.$pluginApi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    // PUT api/reseller/customers/{id}
    update ({}, config) {
      const { id, data, slug = null } = config
      console.log('...Update reseller customers from api')
      return this.$pluginApi
        .$put(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    // Query
    query ({ state, commit }, config) {
      const { slug = null } = config
      const request = state.request
      if (request) {
        request.cancel('fetch cancelled')
        commit('SET_QUERY_REQUEST', null)
      }
      commit('SET_QUERY_LOADING', true)
      console.log('...Fetch reseller roles from api')
      const axiosSource = this.$pluginApi.CancelToken.source()
      commit('SET_QUERY_REQUEST', axiosSource)
      return this.$pluginApi
        .$get(
          resourceURL + (slug !== null ? '?' + slug : ''),
          { cancelToken: axiosSource.token }
        )
        .then((response) => {
          commit('SET_QUERY_LOADING', false)
          commit('SET_QUERY_DATA', response)
          commit('SET_QUERY_REQUEST', null)
          return response
        })
        .catch((error) => {
          commit('SET_QUERY_LOADING', false)
          throw error
        })
    }

  },

  getters: {}
})
