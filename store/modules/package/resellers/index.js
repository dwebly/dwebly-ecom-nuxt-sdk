// store/modules/counter.js
/* eslint-disable no-empty-pattern */

export default options => ({
  storeName: options.namespace + '.resellers', // REQUIRED
  namespaced: true,
  state: () => ({
    target_order_id: null,
    registration: null,
    approved: false,
    reseller_info: null
  }),
  mutations: {
    SET_TARGET_ORDER_ID (state, id) { state.target_order_id = id },
    SET_REGISTRATION (state, data) { state.registration = data },
    SET_APPROVED (state, data) { state.approved = data },
    SET_RESELLER_INFO (state, data) { state.reseller_info = data }
  },
  actions: {
    // FUNCTION SIDE
    setOrderId ({ commit }, id) {
      commit('SET_TARGET_ORDER_ID', id)
      return id
    },
    setRegistration ({ commit }, data) {
      commit('SET_REGISTRATION', data)
      return data
    },
    logoutReseller ({ commit }) {
      commit('SET_APPROVED', false)
      commit('SET_RESELLER_INFO', null)
    }
  },
  getters: {
    getSettings: (state) => (type = 'all') => {
      let data = {}
      switch (type) {
        case 'all':
          data = {
            store_settings: state.storeSettings,
            shipping_settings: state.shippingSettings,
            payment_settings: state.paymentSettings
          }
          return data
        default:
          data[type+'_settings'] = state[type+'Settings']
          return data
      }
    }
  }
})
