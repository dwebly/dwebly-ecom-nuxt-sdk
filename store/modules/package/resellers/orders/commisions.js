// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/order/commisions'

export default (options) => ({
  storeName: options.namespace + '.resellers.orders.commisions', // REQUIRED
  namespaced: true,
  state: () => ({}),

  mutations: {},

  actions: {
    // POST api/reseller/order/commisions
    store ({}, config) {
      const { data, slug = null } = config
      return this.$pluginApi
        .$post(resourceURL + (slug !== null ? '?' + slug : ''), data, { headers: { 'Content-Type': 'application/json' } })
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },

  getters: {}
})
