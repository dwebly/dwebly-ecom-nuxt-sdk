// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/orders'

export default options => ({
  storeName: options.namespace + '.resellers.orders', // REQUIRED
  namespaced: true,
  state: () => ({
    orders: [],
    // Query
    request: null,
    query: null,
    query_loading: false
  }),

  mutations: {
    SET_ORDERS (state, orders) { state.orders = orders },
    // Query
    SET_QUERY_REQUEST (state, request) { state.request = request },
    SET_QUERY_DATA (state, query) { state.query = query },
    SET_QUERY_LOADING (state, loading) { state.query_loading = loading }
  },

  actions: {
    // api/reseller/orders
    fetchAll ({ state, commit }, config) {
      const { slug = null } = config
      const orders = state.orders
      if (orders.length) {
        console.log('...Fetch orders from state')
        return orders
      } else {
        console.log('...Fetch orders from state')
        return this.$pluginApi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_ORDERS', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // GET api/reseller/orders/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      const order = this.getters['resellers/orders/getOrderById'](id)
      if (order) {
        console.log('...Fetch order from state')
        return order
      } else {
        return this.$pluginApi
          .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
          .then((response) => { return response.data })
          .catch((error) => { throw error })
      }
    },

    // Query
    query ({ state, commit }, config) {
      const { slug = null } = config
      const request = state.request
      if (request) {
        request.cancel('fetch cancelled')
        commit('SET_QUERY_REQUEST', null)
      }
      commit('SET_QUERY_LOADING', true)
      console.log('...Fetch reseller roles from api')
      const axiosSource = this.$pluginApi.CancelToken.source()
      commit('SET_QUERY_REQUEST', axiosSource)
      return this.$pluginApi
        .$get(
          resourceURL + (slug !== null ? '?' + slug : ''),
          { cancelToken: axiosSource.token }
        )
        .then((response) => {
          commit('SET_QUERY_LOADING', false)
          commit('SET_QUERY_DATA', response)
          commit('SET_QUERY_REQUEST', null)
          return response
        })
        .catch((error) => {
          commit('SET_QUERY_LOADING', false)
          throw error
        })
    }

  },

  getters: {
    getOrders: (state) => state.orders,
    getOrderById: (state) => (id) => {
      const order = JSON.parse(JSON.stringify(state.orders))
      const findOrder = order.find(x => x.order_id === id.toString())
      return (typeof findOrder === 'undefined') ? false : findOrder
    }
  }
})
