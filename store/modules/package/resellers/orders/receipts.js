// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/order/receipts'

export default (options) => ({
  storeName: options.namespace + '.resellers.orders.receipts', // REQUIRED
  namespaced: true,
  state: () => ({}),

  mutations: {},

  actions: {
    // GET api/reseller/order/receipts
    fetch ({}, config) {
      const { id, slug = null } = config
      return this.$pluginApi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },

  getters: {}
})
