// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/user/auth'

export default options => ({
  storeName: options.namespace + '.resellers.users.authentication', // REQUIRED
  namespaced: true,
  state: () => ({}),

  mutations: {},

  actions: {

    // POST api/reseller/users/auth/change-password
    changePassword ({}, config) {
      const { data, slug = null } = config
      return this.$pluginApi
        .$post(
          resourceURL + '/change-password' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    },

    // POST api/reseller/users/auth/forgot-password
    forgotPassword ({}, config) {
      const { data, slug = null } = config
      return this.$pluginApi
        .$post(
          resourceURL + '/forgot-password' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    },

    // POST api/reseller/users/auth/reset-password
    resetPassword ({}, config) {
      const { data, slug = null } = config
      return this.$pluginApi
        .$post(
          resourceURL + '/reset-password' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response })
        .catch((error) => { throw error })
    }

  },

  getters: {}
})
