// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/user/banks'

export default options => ({
  storeName: options.namespace + '.resellers.users.banks', // REQUIRED
  namespaced: true,
  state: () => ({
    banks: [],
    bank: {}
  }),

  mutations: {
    SET_BANKS (state, banks) { state.banks = banks },
    SET_BANK (state, bank) { state.bank = bank }
  },

  actions: {

    // api/reseller/user/banks
    fetchAll ({ commit }, config) {
      const { slug = null } = config
      console.log('...Fetch orders from state')
      return this.$pluginApi
        .$get(resourceURL + (slug ? '?' + slug : ''))
        .then((response) => {
          commit('SET_BANKS', response.data)
          return response.data
        })
        .catch((error) => { throw error })
    },

    // GET api/reseller/users/{id}
    fetch ({ commit }, config) {
      const { id, slug = null } = config
      return this.$pluginApi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => {
          commit('SET_BANK', response.data)
          return response.data
        })
        .catch((error) => { throw error })
    },

    // POST api/reseller/users/{id}
    store ({ commit }, config) {
      const { data, slug = null } = config
      return this.$pluginApi
        .$put(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          commit('SET_BANK', response.data)
          return response.data
        })
        .catch((error) => { throw error })
    },

    // PUT api/reseller/users/{id}
    update ({ commit }, config) {
      const { id, data, slug = null } = config
      return this.$pluginApi
        .$put(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => {
          commit('SET_BANK', response.data)
          return response.data
        })
        .catch((error) => { throw error })
    },

    // DELETE api/carts/discountCodes/{id}
    remove ({}, config) {
      const { id, slug = null } = config
      console.log('...Removing discount codes')
      return this.$pluginApi
        .$delete(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }

  },

  getters: {
    getBanks: state => state.banks,
    getBankById: state => (id) => {
      const data = JSON.parse(JSON.stringify(state.bank))
      if (data.length === 0) { return false }
      const findData = data.find(x => x.id === id.toString())
      return (typeof findData === 'undefined') ? false : findData
    }
  }
})
