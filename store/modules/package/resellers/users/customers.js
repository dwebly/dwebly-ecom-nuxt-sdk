// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/user/customers'

export default options => ({
  storeName: options.namespace + '.resellers.users.customers', // REQUIRED
  namespaced: true,
  state: () => ({}),

  mutations: {},

  actions: {

    // GET api/reseller/user/customers/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      console.log('...Fetch cart from api')
      return this.$pluginApi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => {
          this.commit('dw/resellers/SET_APPROVED', response?.data?.active)
          this.commit('dw/resellers/SET_RESELLER_INFO', response.data)
          return response.data
        })
        .catch((error) => { throw error })
    }

  },

  getters: {}
})
