// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/reseller/users'

export default options => ({
  storeName: options.namespace + '.resellers.users', // REQUIRED
  namespaced: true,
  state: () => ({}),

  mutations: {},

  actions: {

    // GET api/reseller/users/{id}
    fetch ({}, config) {
      const { id, slug = null } = config
      return this.$pluginApi
        .$get(resourceURL + '/' + id + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    // PUT api/reseller/users/{id}
    update ({}, config) {
      const { id, data, slug = null } = config
      return this.$pluginApi
        .$put(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }

  },

  getters: {}
})
