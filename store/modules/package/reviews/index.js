// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/review'

export default options => ({
  storeName: options.namespace + '.reviews', // REQUIRED
  namespaced: true,
  state: () => ({
    reviews: {}
  }),
  mutations: {
    SET_REVIEWS_BY_TYPE_ID (state, { type, typeId, reviews }) {
      state.reviews = {
        ...state.reviews,
        [type + typeId]: reviews
      }
    }
  },
  actions: {

    // api/reviews
    fetchAll ({}, config) {
      const { id, slug = null, type, full = false } = config
      console.log('...Fetch reviews from api')
      return this.$eapi
        .$get(
          resourceURL + 's?type=' + type + '&type_id=' + id + (slug !== null ? '&' + slug : '')
        )
        .then((response) => { return full ? response : response.data })
        .catch((error) => { throw error })
    },

    // api/reviews
    store ({}, config) {
      const { data, slug = null } = config
      // create new reviews
      console.log('...Create reviews from api')
      return this.$eapi
        .$post(
          resourceURL + 's' + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    // DELETE api/reviews/{id}
    remove ({}, config) {
      const { id, slug = null } = config
      console.log('...delete reviews from api')
      return this.$eapi
        .$delete(
          resourceURL + 's/' + id + (slug !== null ? '?' + slug : ''),
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }

  },
  getters: {
    getReviewsByTypeId: (state) => ({ type, typeId }) => state.reviews[type + typeId]
  }
})
