// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/review/likes'

export default options => ({
  storeName: options.namespace + '.reviews.likes', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // POST api/reviews/likes/{review_id}
    update ({}, config) {
      const { id, data, slug = null } = config
      return this.$eapi
        .$put(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
