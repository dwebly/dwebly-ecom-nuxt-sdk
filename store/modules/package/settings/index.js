// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/settings'

export default options => ({
  storeName: options.namespace + '.settings', // REQUIRED
  namespaced: true,
  state: () => ({
    storeSettings: null,
    shippingSettings: null,
    paymentSettings: null
  }),
  mutations: {
    SET_SETTINGS (state, settings) {
      if (typeof settings.store_settings !== 'undefined') {
        state.storeSettings = settings.store_settings
      }
      if (typeof settings.payment_settings !== 'undefined') {
        state.paymentSettings = settings.payment_settings
      }
      if (typeof settings.shipping_settings !== 'undefined') {
        state.shippingSettings = settings.shipping_settings
      }
    }
  },
  actions: {
    // api/settings
    fetch ({ commit }, config) {
      let slug = null
      if(config){
        slug = config.slug
      }
      let type = 'all'
      if (/type=store/.test(slug)) { type = 'store' }
      if (/type=payment/.test(slug)) { type = 'payment' }
      if (/type=shipping/.test(slug)) { type = 'shipping' }

      const settings = this.getters['dw/settings/getSettings'](type)
      if (type === 'all' && settings.store_settings && settings.shipping_settings && settings.payment_settings) {
        console.log('...Fetch store setting from state')
        return settings
      } else if (settings[type + '_setings']) {
        console.log('...Fetch store setting from state')
        return settings
      } else {
        console.log('...Fetch store setting from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_SETTINGS', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    }
  },
  getters: {
    getSettings: (state) => (type = 'all') => {
      let data = {}
      switch (type) {
        case 'all':
          data = {
            store_settings: state.storeSettings,
            shipping_settings: state.shippingSettings,
            payment_settings: state.paymentSettings
          }
          return data
        default:
          data[type + '_settings'] = state[type + 'Settings']
          return data
      }
    }
  }
})
