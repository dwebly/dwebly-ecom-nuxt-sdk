// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/theme/storelite/settings'

export default options => ({
  storeName: options.namespace + '.themes.settings', // REQUIRED
  namespaced: true,
  state: () => ({
    infos: null,
    subscription_id: null
  }),
  mutations: {
    SET_INFOS (state, infos) { state.infos = infos },
    SET_SUBSCRIPTION_ID (state, id) { state.subscription_id = id }
  },
  actions: {
    // api/theme/storelite/settings
    fetch ({ state, commit, client }, config) {
      const { slug = null } = config
      const themeInfos = state.infos
      if (themeInfos) {
        console.log('...Fetch theme setting from state')
        // change theme color based on theme response
        if (client) {
          document.documentElement.style.setProperty('--store-color-primary', themeInfos.store_settings.store_color)
        }
        return themeInfos
      } else {
        console.log('...Fetch theme setting from api')

        // call getter here
        return this.$pluginApi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {

            // set theme info in state
            commit('SET_INFOS', response.data)

            // set subscription id in state
            commit('SET_SUBSCRIPTION_ID', response.data.subscription_id)

            // change theme color based on theme response
            const theme = response.data
            if (client) {
              document.documentElement.style.setProperty('--store-color-primary', theme.store_settings.store_color)
            }
            return response.data
          })
          .catch((error) => { throw error })
      }

    }
  },
  getters: {
    getInfos: (state) => state.infos,
    getSubscriptionId: (state) => state.subscription_id
  }
})
