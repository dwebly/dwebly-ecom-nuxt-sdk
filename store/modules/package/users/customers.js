// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/customers'

export default options => ({
  storeName: options.namespace + '.users.customers', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // PUT api/users/{id}
    update ({}, config) {
      const { id, data, slug = null } = config
      console.log('...Update user from api')
      return this.$eapi
        .$put(
          resourceURL + '/' + id + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    },

    // POST api/customers
    store ({}, config) {
      const { data, slug = null } = config
      console.log('...Create customer from api')
      return this.$eapi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
