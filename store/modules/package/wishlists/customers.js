// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/wishlist/customers'

export default options => ({
  storeName: options.namespace + '.wishlists.customers', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // api/wishlists/customers
    fetchAll ({ rootState, commit }, config) {
      const { slug = null } = config
      const wishlists = rootState.dw.wishlists.wishlists
      if (wishlists.length) {
        console.log('...Fetch wishlists from state')
        return wishlists
      } else {
        console.log('...Fetch wishlists from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('dw/wishlists/SET_WISHLISTS', response.data, { root: true })
            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // GET api/wishlists/customers/{id}
    fetch ({}, config) {
      const { token, slug = null } = config
      console.log('...Fetch wishlist from api')
      return this.$eapi
        .$get(resourceURL + '/' + token + (slug !== null ? '?' + slug : ''))
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
