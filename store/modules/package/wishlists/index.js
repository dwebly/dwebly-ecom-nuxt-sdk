// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/wishlists'

export default options => ({
  storeName: options.namespace + '.wishlists', // REQUIRED
  namespaced: true,
  state: () => ({
    wishlists: null
  }),
  mutations: {
    SET_WISHLISTS (state, wishlists) {
      state.wishlists = wishlists
    }
  },
  actions: {
    // api/wishlists
    fetchAll ({ state, commit }, { slug = null }) {
      const wishlists = state.wishlist
      if (wishlists.length) {
        console.log('...Fetch wishlists from state')
        return wishlists
      } else {
        console.log('...Fetch wishlists from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_WISHLISTS', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    },

    // GET api/wishlists/{id}
    fetch ({}, { id }) {
      const wishlist = this.getters['dw/wishlists/getwishlistById'](id)
      if (wishlist) {
        console.log('...Fetch wishlist from state')
        return wishlist
      } else {
        console.log('...Fetch wishlist from api')
        return this.$eapi
          .$get(resourceURL + '/' + id)
          .then((response) => { return response.data })
          .catch((error) => { throw error })
      }

    }
  },
  getters: {
    getWishlistById: (state) => (id) => {
      const wishlists = JSON.parse(JSON.stringify(state.wishlists))
      const findWishlist = wishlists.find(x => x.wishlist_id === id.toString())
      return (typeof findWishlist === 'undefined') ? false : findWishlist
    }
  }
})
