// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/areas'

export default options => ({
  storeName: options.namespace + '.worlds.areas', // REQUIRED
  namespaced: true,
  state: () => ({
    areas: {}
  }),
  mutations: {
    SET_AREAS (state, { code, data }) {
      state.areas = { ...state.areas, [code]: data }
    }
  },
  actions: {

    // api/areas
    fetchAll ({ commit }, { countryCode, divisionCode, slug = null }) {
      const code = countryCode + '-' + divisionCode
      const areas = this.getters['dw/worlds/areas/getAreasByCode'](code)
      if (areas) {
        console.log('...Fetch areas from state')
        return areas
      } else {
        return this.$eapi
          .$get(resourceURL + '/' + code + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_AREAS', { code, data: response.data })
            return response.data
          })
          .catch((error) => { throw error })
      }
    },
  
    // Function Side
    updateAreasByDivisions ({ commit }, { countryCode, data }) {
      const code = countryCode
      const divisions = this.getters['dw/worlds/divisions/getDivisionsByCode'](code) ?? []
      if (divisions.length) {
        console.log('...Update areas in state using divisions')
        data.forEach ( (division) => {
          if (division.city) {
            commit('SET_AREAS', { code: countryCode + '-' + division.code, data: division.city })
          }
        })
      }
      return true
    }

  },
  getters: {
    getAreasByCode: (state) => (code) => {
      return state.areas?.[code] ? state.areas[code] : null
    }
  }
})
