// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/cities'

export default options => ({
  storeName: options.namespace + '.worlds.cities', // REQUIRED
  namespaced: true,
  state: () => ({
    cities: {}
  }),
  mutations: {
    SET_CITIES (state, { code, area, data }) {
      state.cities =  {
        ...state.cities,
        [code]: {
          ...state.cities[code],
          [area]: data
        }
      }
    }
  },
  actions: {

    // api/cities
    fetchAll ({ commit }, { countryCode, divisionCode, areaName = null, slug = null }) {
      const code = countryCode + '-' + divisionCode
      const cities = this.getters['dw/worlds/cities/getCitiesByCode']({ code, area: areaName })
      if(cities){
        console.log('...Fetch cities from state')
        return cities
      } else {
        // Check if area name is null (division dont have area)
        slug = (areaName === null) ? slug : ('area=' + areaName + '&' + slug)
        return this.$eapi
          .$get(resourceURL + '/' + code + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            // setting area as null on no area
            commit('SET_CITIES', { code, area: areaName, data: response.data })
            return response.data
          })
          .catch((error) => { throw error })
      }
    },
  
    // Function Side
    updateCitiesByAreas ({ commit }, { countryCode, divisionCode, data }) {
      const code = countryCode + '-' + divisionCode
      const areas = this.getters['dw/worlds/areas/getAreasByCode'](code) ?? []
      if (areas.length) {
        console.log('...Update cities in state using areas')
        data.forEach ( (area) => {
          if (area.city) {
            commit('SET_CITIES', { code: countryCode + '-' + divisionCode, area: area.name, data: area.city })
          }
        })
      }
      return true
    }

  },
  getters: {
    getCitiesByCode: (state) => ({ area, code }) => {
      return state.cities?.[code]?.[area] ? state.cities[code][area] : null
    }
  }
})
