// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/countries'

export default options => ({
  storeName: options.namespace + '.worlds.countries', // REQUIRED
  namespaced: true,
  state: () => ({
    countries: []
  }),
  mutations: {
    SET_COUNTRIES (state, countries) {
      state.countries = countries
    }
  },
  actions: {
    // api/countries
    fetchAll ({ commit }, slug = null) {
      const countries = this.getters['dw/worlds/countries/getCountries']
      if(countries.length > 0){
        console.log('...Fetch countries from state')
        return countries
      }else{
        console.log('...Fetch countries from api')
        return this.$eapi
          .$get(resourceURL + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_COUNTRIES', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    },
    
    // api/countries/{countryCode}
    fetch ({}, { countryCode, slug = null }) {
      const country = this.getters['dw/worlds/countries/getCountyByCode'](countryCode)
      if (country) {
        console.log('...Fetch country from state')
        return country
      } else {
        console.log('...Fetch country from api')
        return this.$eapi
          .$get(resourceURL + '/'+ countryCode + (slug !== null ? '?' + slug : ''))
          .then((response) => { return response.data })
          .catch((error) => { throw error })
      }
    }

  },
  getters: {
    getCountries: (state) => {
      const countries = JSON.parse(JSON.stringify(state.countries))
      return countries
    },
    getCountryByCode: (state) => (countryCode) => {
      const countries = JSON.parse(JSON.stringify(state.countries))
      const findCountry = countries.find(x => x.code === countryCode.toString())
      return (typeof findCountry === 'undefined') ? false : findCountry
    }
  }
})
