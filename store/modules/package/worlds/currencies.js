// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/currencies'

export default options => ({
  storeName: options.namespace + '.worlds.currencies', // REQUIRED
  namespaced: true,
  state: () => ({
    currencies: []
  }),
  mutations: {
    SET_CURRENCIES (state, currencies) {
      state.currencies = currencies
    }
  },
  actions: {
    // api/currencies
    fetchAll ({ state, commit }, config) {
      const { slug = null } = config
      const currencies = JSON.parse(JSON.stringify(state.currencies))
      if (currencies.length > 0) {
        console.log('...Fetch currencies from state')
        return currencies
      } else {
        console.log('...Fetch currencies from api')
        return this.$eapi
          .$get(resourceURL + '?' + slug)
          .then((response) => {
            commit('SET_CURRENCIES', response.data)
            return response.data
          })
          .catch((error) => { throw error })
      }
    }
  },
  getters: {
    getCurrencies: (state) => {
      const currencies = JSON.parse(JSON.stringify(state.currencies))
      return currencies
    }
  }
})
