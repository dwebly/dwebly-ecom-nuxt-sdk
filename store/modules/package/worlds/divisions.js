// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/divisions'

export default options => ({
  storeName: options.namespace + '.worlds.divisions', // REQUIRED
  namespaced: true,
  state: () => ({
    divisions: {}
  }),
  mutations: {
    SET_DIVISIONS (state, { code, data }) {
      state.divisions = { ...state.divisions, [code]: data }
    }
  },
  actions: {
    // api/divisions
    fetchAll ({ commit }, { countryCode, slug = null } ) {
      const code = countryCode
      const divisions = this.getters['dw/worlds/divisions/getDivisionsByCode'](code)
      if (divisions) {
        console.log('...Fetch divisions from state')
        return divisions
      } else {
        console.log('...Fetch divisions from api')
        return this.$eapi
          .$get(resourceURL + '/' + countryCode + (slug !== null ? '?' + slug : ''))
          .then((response) => {
            commit('SET_DIVISIONS', { code, data: response.data })
            return response.data
          })
          .catch((error) => { throw error })
      }
    }
  },
  getters: {
    getDivisionsByCode: (state) => (code) => {
      return state.divisions?.[code] ? state.divisions[code] : null
    }
  }
})
