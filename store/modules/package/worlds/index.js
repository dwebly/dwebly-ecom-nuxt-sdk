// store/modules/counter.js
/* eslint-disable no-empty-pattern */

export default options => ({
  storeName: options.namespace + '.worlds', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {},
  getters: {}
})
