// store/modules/counter.js
/* eslint-disable no-empty-pattern */

const resourceURL = '/location/mappers'

export default options => ({
  storeName: options.namespace + '.worlds.locations.mappers', // REQUIRED
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    // api/location/mappers
    check ({}, config) {
      const { data, slug = null } = config
      console.log('...Map location from api')
      return this.$eapi
        .$post(
          resourceURL + (slug !== null ? '?' + slug : ''),
          data,
          { headers: { 'Content-Type': 'application/json' } }
        )
        .then((response) => { return response.data })
        .catch((error) => { throw error })
    }
  },
  getters: {}
})
